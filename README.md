## Todo

-   [x] Talent Bundles
-   [ ] Armor Placement (Positional)
-   [x] Import/Export Characters to json
-   [x] Cloud Saves
-   [ ] Render Character Sheet (PDF)
-   [x] Add React-Router ie. `/characters/:id/stats`
-   [x] Store/serve Markdown (for game rules, etc)
-   [ ] Pick inventory (encumberence points? weapon tags?)

### Rules Todo
-   [ ] Magic/Runes Rules (how to cast spell, what runes "generally" do)
-   [ ] Skill Checks: "roll stat + associated skill" of d6's, 5-6 are success
    - rolled against GM's DC
-   [ ] Combat flow
    - 3 actions per turn
    - additional attacks per turn have less dice (unless talents)
    - use opposed Skill Checks (maybe streamlined to averages for npcs?)
-   [ ] Weapons
    - eg. Great Weaponmaster feat should be attached to the Weapon type itself
-   [ ] Health/Damage/Armor system
    - positional damage/armor is cool?
    - merge stun/meat damage tracks (run out = unconscious, fill with meat = ded)
-   [ ]
-   [ ]
-   [ ]
-   [ ]
-   [ ]

## API Spec

-   `GET /characters` - lists all characters
    -   `{characters: [{id: 'blah@gmail.com_Bob-human', name: 'Bob-human'}]}`
-   `GET /characters?email=blah` - filters to a single user's characters
-   `GET /characters/:id` - returns all a character's info (full state blob)
    -   `{character: {race: 'human', ...}}`
-   `PUT /characters/:id` - saves a character's info
    -   `{race: 'human', ...}`
