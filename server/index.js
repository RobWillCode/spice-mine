const express = require('express')
const cors = require('cors')
const filenamify = require('filenamify')
const fsPromises = require('fs').promises
const path = require('path')

const app = express()
const listenPort = 3001

app.use(express.json())
const corsOptions = {
    allowedHeaders: ['Content-Type', 'Authorization'],
    maxAge: 3600,
}
app.use(cors(corsOptions)) // enable CORS on all routes
app.use((request, response, next) => {
    console.log(`- ${request.method} ${request.originalUrl}`)
    next()
})

app.get('/', (request, response) => {
    response.json({ pizza: 'meat-treat' })
})

const charactersDir = path.join(__dirname, '..', 'characters')
fsPromises.mkdir(charactersDir, { recursive: true })

const fileNameSplitRegex = /(.*@.*?)_(.*)/
app.get('/characters', async (request, response) => {
    const filenamePrefix = request.query.email
        ? filenamify(request.query.email + '_')
        : ''

    response.json({
        characters: (await fsPromises.readdir(charactersDir))
            .filter(file => !filenamePrefix || file.startsWith(filenamePrefix))
            .map(file => {
                const [_, email, name] = file.match(fileNameSplitRegex)
                return {
                    id: file,
                    name,
                    email,
                }
            }),
    })
})

app.get('/characters/:id', async (request, response) => {
    const { id } = request.params

    const filename = path.join(charactersDir, filenamify(id))
    const contents = await fsPromises.readFile(filename)
    response.json({
        character: JSON.parse(contents),
    })
})

app.put('/characters/:id', async (request, response) => {
    const { id } = request.params

    await fsPromises.mkdir(charactersDir, { recursive: true })
    await fsPromises.writeFile(
        path.join(charactersDir, filenamify(id)),
        JSON.stringify(request.body)
    )
    response.json({ status: 'ok' })
})

app.listen(listenPort)
console.log(`Server listening on port ${listenPort}`)
