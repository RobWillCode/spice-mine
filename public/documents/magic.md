# Magic
Magic in Spice Mine is inherently unstable, casters draw their power from the glow (radiation like particles) that is carried in the air and permeates the earth and water. While the glow provides a nearly unlimited source of power it carries a cost on those that would channel it. With every spell cast a character exposes themselves to increasing levels of taint that over time will mutate their bodies and erode their sanity.
To wield magic, energy from the Glow must be channelled through so-called "runes" known by the caster. The runes used in turn determine the exact nature and effect of the spell.

## Spellcasting
Any creature may cast a spell as an action as long as it has:  
-A magic attribute of at least 1.  
-At least 1 source rune and 1 form rune  

Spellcasting can be broken down into 4 steps:

### 1. Select runes and determine spell effect.  
First the caster chooses a single source rune and a single form rune. They may also add a single additional Meta rune*. Once all runes are chosen, the player can describe the general effects of the spell (eg. throw a fireball, turn invisible, create an ice barrier), however the GM has final say in what is admissible. The GM then uses the applied runes to determine the <i>specific</i> nature of the spell, such as the range, damage dealt, status effects applied etc.
>*Certain abilities and effects may allow you to add additional meta runes to a spell beyond the first.

### 2. Pay costs  
Determine the cost of the spell by adding the costs from each of the chosen runes to the base cost. The base cost to cast a spell is 1 action. All costs must be paid before moving to step 3.  

If during this step the caster is unable to pay the spell costs for any reason, the spell fails. The caster must still make a Spellcasting test, but ignores all successes. When making a Spellcasting test for a failed spell, all misses increase taint by 2.

### 3. Make a spellcasting test  
Once the spell effect(s) have been determined and costs paid, the caster must test <b>Spellcasting</b> with a difficulty equal to the spells complexity statistic. The number of hits attained on this test become the spells <b>Strength</b> attribute. If the spell requires an attack roll, the spellcasting test doubles as the attack test against the target. In this case, resolve the attack action immediately before moving to step 4.  

Casting multiple spells in quick succession can be challenging. As such, if the caster has already cast at least one spell this turn, they incur a negative modifier to each spellcasting test after the first. The modifier is equal to The total complexity of all previous spells cast this turn, plus the total number of previous spells cast.  

><b>Example</b>  
>A caster tries to cast 3 spells in quick succession. The first has a complexity of 1, the second a complexity of 2, and the third a complexity of 1. The caster has a Spellcasting pool of 8. Their first spell recieves no negative modifiers so they roll 8 dice on the Spellcasting test. The second Spellcasting test receives a penalty of 2(1 total complexity + 1 previous spell cast), so they roll 6 dice. The third Spellcasting test recieves a penalty of 5(3 total complexity + 2 previous spells cast), so they only roll 3 dice.

### 4. Increase Taint  
Whether the spell succeeds or not, the caster gains 1 Taint for each miss on the spellcasting test.

### Drawing on Chaos  
Magic users draw on the power of the Glow to fuel their spells. Controlling how much power they take in is challenging, and is something every magic user learns early on. However, a caster can also choose to allow the full force of the Glow's chaotic energy to flow into their spells, letting them wield far more powerful spells than normal. This comes at a great risk, as the energy mutates the bodies and minds of those who channel it.

During a spellcasting test, before the dice are rolled, the caster may choose to <b>Draw on Chaos</b>. They add any number of Chaos dice to their dice pool up to half their base pool amount. These dice must be visually distinct from their other pool dice, or rolled separately. Chaos dice count 4, 5 and 6 as a success on spellcasting tests, but also count 1, 2 and 3 as misses. Chaos dice are otherwise treated the same as normal dice, including increasing taint on misses.

### Sustaining Spells
Most spells are cast and resolved immediately in the same action. However if one of the runes used to construct a spell has the <b>Sustained</b> spell attribute, that spell can be maintained by the caster over multiple turns. When a sustained spell is cast, it lasts until the start of the casters next turn. At the start of the casters turn, instead of allowing the spell to expire they can spend an action to sustain it for an additional turn. As long as they continue spending actions in this way, a spell can be sustained indefinitely.  
If a character that is sustaining a spell takes damage, they must succeed at a <b>Volition</b> test with a difficulty equal to (TODO - some formula involoving damage taken. Need the health system fleshed out first) or the sustained spell immediately ends. 

## Rune Types

### Form Runes
Form runes are what determine the shape of the spell, and thus will have the biggest impact on the nature of the spell you eventually create. Form runes usually (though not always) indicate:  
-The spells range and number of targets or area affected.  
-The spell effect, such as inflicting damage, granting a buff, creating an object, etc.  
-Spell attributes, such as if the spell requires an attack roll to hit it's target, or requires concentration to cast.  
-Complexity (this is a measure of the spills difficulty to cast)  
-Additional costs to cast the spell (this can include extra actions, sacrificing health or sanity, etc.)  

Some example Form runes:  
-Projectile  
-Beam  
-Imbue  
-Barrier  
-Conjure  

### Source Runes
Source runes dictate the type of energy being used in the spell. Form runes usually (though not always) indicate:  
-The type of damage associated with the spell  
-The status effect associated with the spell  
-Spell attributes, such as if the spell requires an attack roll to hit it's target, or requires concentration to cast.  
-Additional rules associated with the spell (for example, the Illusion source rune indicates that the spell does no damage)  
-Additional costs to cast the spell (this can include extra actions, additional complexity, sacrificing health or sanity, etc.)  
>It is important to note that not every spell will necessarily use each of these statistics.  

### Meta Runes
Meta runes are optional runes that can be added to a complete spell in order to modify it in different ways. Some meta runes provide a straightforward buff to the spell, such as increasing any spells range by 50%. Others change the spell in more dramatic ways, such as giving autonomy to the spell effect. It is up the the player to come up with a plausible outcome when such runes are applied (with GM approval of course).

Meta runes can add Spell Attributes to a spell, just like any other rune, and many runes will add additional costs or complexity to the final spell as well.

## Channelling
Spellcasters can choose to spend more time on the preparation and casting of a spell, making it easier for them to perform the spell. Some more powerful spells will require channelling ahead of time, as they cost more actions than a character gets in a single turn.

To channel a spell, the caster simply declares that they are spending one or more actions Channelling. The maximum number of actions that can be spent channelling a single spell is equal to the casters <b>Magic</b> attribute. As long as no actions are performed between channelling and casting a spell, the caster gains the benefits of channelling for the next spell they cast. For each action spent channelling, the caster can choose one of the following benefits:  
-Reduce the number of actions required to cast the spell by 1, to a minimum of 1.  
-Add 2 dice to their spellcasting test for the spell.  
-Add 1 additional meta rune to the spell.  

Channelling can span turns, as long as the caster does not perform any other actions between channelling and casting the spell. If a character that is channelling takes damage, they must succeed at a <b>Volition</b> test with a difficulty equal to (TODO - some formula involoving damage taken. Need the health system fleshed out first) or the channelling is ended and any benefits lost. 

## Dispelling
The dispelling skill allows a character to destroy sustained or existing magical effects, or to counter spells being cast by another character. When rolling <b>Dispelling</b> tests, misses increase taint just like when casting a spell. Note that when using <b>Dispelling</b> characters CANNOT draw on chaos to increase their dice pool.

### Dispel
To dispel a sustained spell or magical effect, the character must spend an action and make a <b>Dispelling</b> test. For each success rolled, the target spell or effect has it's <b>strength</b> statistic reduced by 1. If a spell or effect ever has a strength less than 1, it immediately ceases to exist.

### Counter Spell
Countering another casters spell is simply Dispelling the spell at it's point of origin. The character must first hold a Dispel action on their turn, requiring an action as normal. Once a character they can see successfully casts a spell, but before the spells effects are applied, the dispelling character may make a <b>Dispelling</b> test as above. For each success rolled on the <b>Dispelling</b> test, reduce the successes of the targets <b>Spellcasting</b> test by 1. If this would cause the caster to not have enough successes to cast the spell, the spell fails. 

## Assensing


## Implements
An implement is any physical object which has been enhanced to make it particularly adept at channeling energy from the Glow. All standard implements are imbued with one or more runes, and allow any caster who wields the implement to cast spells using those runes, even if they do not possess it. 

Enhanced implements are especially rare or powerful implements that bestow other benefits to the caster, such as enhancing the effects of channelling, allowing for additional meta runes to be added to a spell, or letting a caster draw upon more chaos dice than usual. Enhanced implements are not available at character creation, and must be acquired during play.

>Benefits from implements should generally involve a trade off of some kind or encourage different tactics, not just be flat modifiers or extra dice on tests.

## Background Count
The Glow permeates the air and ground, making spellcasting possible almost anywhere on the planet. However the strength of the Glow can change in different areas or parts of the world. The strength of the Glow in any one location is known as the <i>Background Count</i>. In game terms, background count is always measured as a plus or minus modifier, with the typical background count found throughout most of the wastes being +0. Backround counts generally range from +4 to -4.
Some examples of things that affect background count are:  
-A radioactive crater (+2)  
-Inside a heavily shielded facility (-4)
-The ruins of a pre-war power plant (+3)
-A building protected by makeshift or scavanged shielding (-1)

