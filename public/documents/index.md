- [Combat Rules](combat.md)

# Spice-Mine
 * [Section 1: The Game](#the-game)
 * [Section 2: Character Creation](#character-creation)
 * [Section 3: Session Structure](#session-structure)
 * [Section 4: Combat](#combat)
 * [Section 5: Magic](#magic)
 * [Section 6: Mutation](#mutation)
 * [Section 7: Compendium](#compendium)
 * [Section 8: GM'S Tools](#gms-tools)
 * [Section 9: Lore](#lore)


## Section 1: The Game
### Introduction

### Playing Spice-Mine
#### What You Need
This game only uses six-sided dice **(d6)**.
You’ll roll these dice to determine the outcome of uncertain situations, such as firing a weapon, calming a machine spirit, or scaling a sheer cliff face.
When the rules call for you to make a roll, it will also tell you how many dice to roll.
For example, **1d6** means you need to roll a single **d6**, whereas **2d6** means you need to roll two **d6s**.

Sometimes the rules will call for you to roll **1d3**.
That’s just a shorthand way of saying you should roll **a 1d6 and halve the result** (rounded up).
When you’re called on to roll 1d3, a result of 1 or 2 on a **d6** equals 1, 3 or 4 equals 2, and 5 or 6 equals 3.

Each player and GM will need a number of **d6's**, probably a dozen or two per person.
You will also need some different colored dice to use as chaos dice.
Each player can either use their own, extra dice, have a shared pool people can use, or just ask the person to your right.
If that person is your GM ask the person to their right (you can never be to careful).

The combat will likely make more sense if you use a grid, so some grid paper or a battle mat would be recommended.

Also if it ever gets completed Spice-Mine should have a companion web app, would recommend using if it exists.

#### Golden Rules
There are two golden rules to remember when reading this book and playing Spice-Mine:

**I: Specific rules override general statements and rules.**

For example, when you shoot at an enemy, your roll is
normally influenced by whether they’re in cover;
however, weapons with the Seeking tag ignore cover.
Because the seeking tag is a specific rule, it
supersedes the general rules governing cover.

**II: Always round up (to the nearest whole number).**

#### Skill Checks
You make skill checks when your character is in a challenging or tense situation that requires effort to overcome.
When you want to act in such a situation, state your objective (e.g., break down the door, decrypt the data, or sweet-talk the guard), then roll 1d20, and add any relevant bonuses.
On a total of 10+, you succeed

##### Simple Tests
To do a simple test roll you dice pool for your relative skill or specialisation and count your hits.
If you meet or beat the threshold set by your GM the action succeeded.

##### Opposed Tests
If a player is being actively opposed by another character, device or object then both sides should roll their dice pools.
Whichever side scores the most hits succeeds. 
If there's a tie the success will normally go to the aggressor.

##### Teamwork Tests
Some times players will want to team up to work on the same action with complementary skills.
To perform a test as a team first nominate a character as the leader who will perform the test.
For every other helper on the test they must roll a dice poll for a complimentary skill, for every hit they get the leader may add a dice to their pool when they roll.
Then the leader will take their test, they can't add more dice from team work than the skill rank (or specialisation rank).
If a helper rolls a mishap on a test they should cause significant distraction to the next action or few actions they take, preventing them from gaining or spending edge.

Helpers should be encouraged to use a variety of skills when assisting a team work test.
When a helper tries to use a different skill than the leader of the test and gives a good reason why this would make sense feel free to award the leader of the test and edge for the upcoming test.

###### Teamwork Tests in Combat
To assist another character while acting in Combat you have to take the assist action on your turn before the team leaders turn.
The team leader must perform their part of the test during their next turn or any benefits are lost.

Trying Again
------------
[//]: # (Option 1: this would seem to encourage players to give a test there best shot, and not just hope they can brute force their way past challenges)
Characters can not retry a test they have failed until either:
1. a significant amount of time has passed (GM's discretion).
2. the characters situation has changed significantly (e.g. gaining blackmail on a target you previously attempted to intimidate).

[//]: # (Option 2: this version would be more forgiving, not sue if the modifier is high enough)
Assuming circumstances allow it characters can attempt an action again if they fail, but for each additional attempt add a cumulative -2 die poll modifier.
Taking a significant break (GM's discretion will reset the pool).

Minor & Major Mishaps
-------
If more than half the dice you roll on a test are 1s, you have rolled a minor mishap.
If you rolled a minor mishap and didn't get a single hit on the test, upgrade that to a major mishap.

### Minor Mishap
Rolling a minor mishap means something went wrong.
It should be inconvenient rather than disastrous, dropping a weapon after swinging with it, or briefly forgetting your assumed name after someone is fooled by your disguise.
If you rolled enough hits, a test with a mishap may also be a success. The mishap doesnt cancel the success, but it causes a problem afterward that you need to recover from. 

### Major Mishap
A major mishap is significantly worse than a minor.
It’s the errant gunshot that misses its target and ricochets back to hit one of your teammates, or the attempted illusion that turns neon pink when you just wanted hide from patrolling guards.
It should be difficult and could have lasting ramifications, but it shouldn’t be fatal, and it shouldn’t penalize the character so much that they stop having fun.
(Having a caster permanently lose several points off their Magic rating would be an example of a no-fun major mishap).
Use major mishaps to advance stories with an air of desperation without pummeling the players into hopelessness.

Edge
----
[//]: # (This is basislly pulled out of shadowrun 6th, really like the idea of this and may try to integrate into more systems)
One way to think of Edge is building up tactical advantages that culminate in a sudden move toward victory. 
It’s both what you plan for and the unexpected moment when you seize an opportunity and make it your own.

### Gaining Edge
Gear, spells, qualities, and other items provide bonus Edge or cancel Edge given to other characters.

[//]: # (SR6 has a number of ways to spend edge by default, those could probaly be ported over, but also thinking of making it so certain gear and traits add more edge sinks).

#### Adventures, Downtime, and Scenes

## Section 2: Character Creation

### Genuses
It is said that all the peoples of the wastes in the time before the calamity were one people, but with  

#### Humans
Adaptive humans, often with a mutation or altered limb. Capable of learning most martial disciplines.

#### Reptiloides
Children of the nuclear fire, heirs to Godzilla

#### Mole Men
Humanoids that look like naked mole rats, they live under the ground and hate sun light

#### Fungaloids
Tainted humans who have gained functional immortality at the cost of slowy turning into living mushrooms

#### Tinmen
Plug-n-Play people, more machine than flesh

#### Marshmen
Use tribal magiks to make others stay outta their swamp.

#### Ankonorai (really need a better name)
For years they have studied the blade. Have two hearts. Proud, arrogant, independent mercenaries. Be wary of their caravans in the salt flats

#### Men of the West
Humans separated from the rest of the world, still capable of using some technology. Covets Archeo Tech, and highly suspicious of those who display taint.

## Section 3: Session Structure

## Section 4: Combat

### Starting Combat
To begin combat, the GM merely needs to declare it.
Hostile moves like firing at enemies, grappling, and charging are usually enough to initiate combat.
When the GM declares that combat is about to start, they show the map to the players (at least the parts their characters can see), explain the position of any visible NPCs or terrain features, and describe the situation.
The players then place their characters on the map, wherever is appropriate, and the first turn begins.

### Ending Combat
Combat ends when one side has carried out their main objective – usually the defeat, destruction, or rout of the other side, but not always.
Combat doesnt always end with the total destruction of one side or another; it’s perfectly fine for a GM to end combat early and return to narrative play if there are no questions as to the outcome.
NPCs often have objectives of their own and tend to be concerned with self-preservation.
Surrender and retreat are perfectly valid end-conditions for a fight.

### Turns and Rounds
Combat is separated into turns and rounds.
Each character takes one turn per round unless they otherwise specified.
A round ends when every character capable of taking a turn has taken a turn.
Turns represent activity, not the passing of time. Even though characters act in a certain order, the turns in each round are assumed to narratively occur at roughly the same time.

Players always get to act first. When combat begins, the players agree on a player (or an allied NPC) to take the first turn.
If the players can’t agree on someone, the GM chooses.
Next, the GM chooses a hostile NPC to act, followed by a player (or allied NPC) nominated by the player who acted previously.
This is followed by another hostile NPC of the GM’s choice, then another player, and so on, alternating between hostile and allied characters until every character has acted.
If all characters on one side have acted, the remaining characters take their turns in an order decided either the GM or the players, as relevant.

When every character has taken a turn, the round ends and a new one begins.
The turn order continues to alternate, so if one side took the last turn in the previous round, the other side starts the new round.
This can result in hostile NPCs acting first in a new round.

### Harm
#### Damage
Every character hopes to avoid getting hurt, but like every cop 3 days from retirement they'll eventually get hit.

There are the following types of damage that characters will need to deal with:
 * kinetic ⚔
 * explosive 💣
 * energy 🗲
 * burn 🔥

#### Armor, Resistance and Immunity
**Armor** reduces all incoming damage from a single source by an amount equal to it's rating; however, **AP** weapons and **Burn 🔥** damage ignore **Armor** altogether.

Characters with **Resistance** to a specific type of damage reduce all incoming damage of that type by half.
Characters can only have **Resistance** once per type of damage - it doesn't stack.

Some characters and objects have **Immunity**, and can’t be affected by certain damage types, attacks, or effects.
**Immunity** goes beyond simply ignoring damage – effects or actions that a character has **Immunity** to are completely ignored, and may as well have failed or not having taken place at all.
For example, a character with **Immunity** to burn doesn’t take any burn from attacks and never counts as having taken burn for the purposes of any other effects.
Likewise, a character with **Immunity** to damage never takes damage (even 0 damage).

#### Calculating damage
After an attacker has successfully hit with an attack, the total damage is calculated in the following order.
1. The attacker rolls damage and adds any net hits. Add any additional relevant reductions or increases from the attacker (such as the target being vulnerable).
2. The targets armor is subtracted from the damage.
3. Any other deductions from the defender are subtracted from the remaining damage. This includes any resistance, or traits or spells.
4. Remaining damage is subtracted from the targets HP.

##### Injuries
When a character's hp is brought down to zero (keep track of any overflow), mark a injury on their sheet, then roll a **d6** for every injury they've incurred.
Compare the lowest result on the injury chart and resolve it.

|     Value    | Level of Damage | Result  |
| ------------ |:---------------:| -------:|
|          5-6 | Grazed          | The character is hurt and is **Impaired** till the end of their next turn |  (some sort of disadvantaged state)
|          2-4 | Brutal          | The character takes a temporary injury (as in one it will heal, like a broken bone). Roll on the temporary Injury chart |
|            1 | Deadly          | The character takes a permanent injury (everything from losing an arm, eye, or their life), Roll on the permanent Injury chart | They should have to be injured a number of times before death happens, also lost limbs and other long term injuries should be able to overcome. It may just take some archeo tech or mutation.
| Multiple 1's | RIP             | If your forgot how to roll a new character, now would be the time to familiarize yourself | (Not sure if this is to harsh)

If their still alive that reset their hp to their starting value, then apply any overflow (This does mean if a character takes a significant amount of damage they could take multiple injuries in a row).

[//]: # (Need to make some injury charts, this should be fun)

#### Burn
Characters need to worry about more than just bullet holes on the battlefield.
Some weapons deal burn 🔥 (damage over time).
Burn might represent flames, acid or something more insidious.

When characters take burn, it has two effects:
first, they immediately take burn damage, ignoring ARMOR, and then they mark down the burn they just took on their sheet.
At the end of their turn, characters with burn marked must roll an {insert relevant skill here} check.
On a success, they clear all burn currently marked; otherwise, they take burn damage equal to the amount of burn currently marked.

Burn from additional sources adds to the total marked burn, so a character that is hit by two separate 2 🔥 attacks first takes 4 burn damage (2 from each attack), then marks down 4 burn (again, 2 from each attack).
At the end of their turn, that character makes an {insert relevant skill here} check, failing and taking an additional 4 damage.
Next turn, the same character gets hit by another 2 🔥 attack.
They take 2 damage, then mark the extra burn down (now it’s 6!).
At the end of their turn,they must succeed on another {insert relevant skill here} check or take 6 more burn damage.
Fortunately they pass, clearing all burn.

#### Taint
**Taint ☣** is a special type of harm that doesn’t count as damage and ignores **Armor**, although it can be affected by **Resistance**.
It represents the warping effects of the Glow upon a characters body and mind.
It's most often inflicted by magic, most often afflicted upon the caster from the stress of casting a spell.
A character that takes taint marks it on their sheet.
When it reaches its **Taint Cap**, any additional taint causes it to mutate.
Mutation is discussed in more detail in [Section 6](#mutation).

If a character without a **Taint Cap** would take taint, they instead
take an equivalent amount of energy 🗲 damage.

### Actions
This section describes the different actions available
to characters, and how they work.

Characters can take up to 3 actions on their turn.

[//]: # (Should there be a restriction on the number of times an action can be taken?)

## Section 5: Magic
Magic in Spice Mine is inherently unstable, casters draw their power from the glow that is carried in the air and permeates the earth and water.
While the glow provides a nearly unlimited source of power it carries a cost on those that would channel it.
With every spell cast a character exposes themselves to increasing levels of taint that over time will mutate their bodies and erode their sanity.
To wield magic, energy from the Glow must be channelled through so-called "runes" known by the caster.
The runes used in turn determine the exact nature and effect of the spell.

### How to Cast a Spell
Any character may cast spells as long as it has the following:
 * A magic attribute of at least 1
 * At least 1 Elemental Rune
 * At least 1 Form Rune
 
To cast the spell they then follow these steps:

#### 1. Select Runes
First the caster chooses one **Elemental Rune** and one **Form Rune**, they may optionally add a **Meta Rune***.
Once all runes are chosen, the player can describe the general effects of the spell (eg. throw a fireball, turn invisible, create an ice barrier), however the GM has final say in what is admissible.
The GM then uses the applied runes to determine the *specific nature* of the spell, such as the range, damage dealt, status effects applied etc.

> \* **Meta Runes**: certain actions and traits may allow the user to use more than one **Meta Rune** on a spell.

#### 2. Pay Costs
The default cost of a spell is one action.
Different **Form Runes** can increase the cost of the spell, while **Meta Runes** can either increase or decrease the cost.
The most common cost is additional actions, but could also include taking damage, incurring taint, or more esoteric costs.

If during this step the caster is unable to pay the spell costs for any reason, the spell fails.
The caster must still make a **Spellcasting** test, but ignores all successes.
When making a **Spellcasting** test for a failed spell, all misses incur 2 **Taint ☣** damage.

[//]: # (Not sure how likely this is to happen, and concerned if it's to harsh?)

#### 3. Cast the Spell
Once the spell effect(s) have been determined and costs paid, the caster must make a simple skill check using **Spellcasting** (if your targeting an enemy instead do a contested check, vs a defensive skill determined by the element/form).
The number of hits attained on this test become the spells **Strength** attribute.
If the spell requires an attack roll, the **Spellcasting** test doubles as the attack test against the target.
In this case, resolve the attack action immediately before moving to step 4.

#### 4. Increase Taint
Whether the spell succeeds or not, the caster takes 1 **Taint ☣** damage for each miss on the **Spellcasting** test.

> This damage ignores any **Resistance** and **Immunity** the caster may have. 

### Types of Runes

#### Elemental Runes

---

##### Fire Rune
Damage Type: **Burn 🔥**
Tags:

##### Radiation Rune
Damage Type: **Energy 🗲**
Tags: AP (Armor Piercing)
[//]: # (Need a section for tags, but AP should ignore armor completely when dealing damage).

##### Stone Rune
Damage Type: **kinetic ⚔**



#### Form Runes

---
##### Touch Form
Range: 1, single target
Damage: 1d6
Cost: 1 Action
Tags: 

##### Bolt Form
Range: 10, single target
Damage: 1d6
Cost: 1 Action

##### Blast Form
Range: 10, blast 1
Damage: 1d3
Cost: 2 Action

##### Burst Form
Range: 0, blast 2
Damage: 1d3
Cost: 2 Action

##### Beam Form
Range: 5, line 1
Damage: 1d6+SP
Cost: 2 Action

##### Barrier Form
Range: 0, burst 1
Armor: SP
Resistance: Energy Type of the Element

The caster surrounds the area around themselves with a protective wall.
The wall appears at the edge of the burst template, you can choose for which parts of the wall to manifest (you must have at least one section manifest).

Any time a ranged attack or spell attack would pass thru the wall reduce the damage dealt by the walls spell strength.
If the wall if of the same damage type as the attack half the reduced damage.
Reduce the SP of the wall by 1.

If a character moves through a hostile wall they take 1d6 damage of the walls type.
Reduce the SP of the wall by 1.

[//]: # (Should the damage for walking through be equal to the SP?)
[//]: # (Should armor work against wall damage?)

At the beginning of the casters turn reduce the SP of the wall by 1.

```
.....   .....   .....   .bbbbb.
.....   .bbb.   .bbb.   .b...b.
..@..   .b@b.   ..@..   .b.@.b.
.....   .bbb.   .....   .b...b.
.....   .....   .....   .bbbbb.
```

##### Imbue Form


#### Meta Runes

##### Empower Rune
Cost: 1 action
Effect: Add and additional die based on the Elemental Type

##### Seeking Rune
Cost: 1 action
Effect: 

---

### Overcast
Magic users draw on the power of the Glow to fuel their spells.
Controlling how much power they take in is challenging, and is something every magic user learns early on.
However, a caster can also choose to allow the full force of the Glow's chaotic energy to flow into their spells, letting them wield far more powerful spells than normal.
This comes at a great risk, as the energy mutates the bodies and minds of those who channel it.

During a **spellcasting** test, before the dice are rolled, the caster may choose to **Overcast**.
They add any number of Chaos dice to their dice pool up to half their base pool amount.
These dice must be visually distinct from their other pool dice, or rolled separately.
Chaos dice count 4, 5 and 6 as a success on **spellcasting** tests, but also count 1, 2 and 3 as misses.
Chaos dice are otherwise treated the same as normal dice, including causing **Taint ☣** damage on misses.

### Channeling

### Dispelling
The dispelling skill allows a character to destroy sustained or existing magical effects, or to counter spells being cast by another character.
When rolling **Dispelling** tests, misses increase taint just like when casting a spell.
Note that when using **Dispelling** characters **Overcast** to increase their dice pool.

#### Dispel
[//]: # (Probably need a different word, makes it hard to differentiate)
To dispel a sustained spell or magical effect, the character must spend an action and make a **Dispelling** test.
For each success rolled, the target spell or effect has it's **SP** statistic reduced by 1.
If a spell or effect ever has a strength less than 1, it immediately ceases to exist.

#### Counter Spell
Countering another casters spell is simply Dispelling the spell at it's point of origin.
The character must first hold a Dispel action on their turn, requiring an action as normal.
Once a character can see successfully casts a spell, but before the spells effects are applied, the dispelling character may make a **Dispelling** test as above.
For each success rolled on the **Dispelling** test, reduce the successes of the targets **Spellcasting** test by 1.
If this would cause the caster to not have enough successes to cast the spell, the spell fails. 

### Assensing

### Implements
An implement is any physical object which has been enhanced to make it particularly adept at channeling energy from the Glow.
All standard implements are imbued with one or more runes, and allow any caster who wields the implement to cast spells using those runes, even if they do not possess it. 

Enhanced implements are especially rare or powerful implements that bestow other benefits to the caster, such as enhancing the effects of channelling, allowing for additional meta runes to be added to a spell, or letting a caster draw upon more chaos dice than usual.
Enhanced implements are not available at character creation, and must be acquired during play.

[//]: # (Only not sure how I feel about gear that can only be acquired thru play, may need to think on that)
[//]: # (Benefits from implements should generally involve a trade off of some kind or encourage different tactics, not just be flat modifiers or extra dice on tests.)

### Background Count
The Glow permeates the air and ground, making **spellcasting** possible almost anywhere on the planet.
However the strength of the Glow can change in different areas or parts of the world.
The strength of the Glow in any one location is known as the **Background Count**.
In game terms, background count is always measured as a plus or minus modifier, with the typical background count found throughout most of the wastes being +0.
Background counts generally range from +4 to -4.

Some examples of things that affect background count are:  
 * A radioactive crater (+2)  
 * Inside a heavily shielded facility (-4)
 * The ruins of a pre-war power plant (+3)
 * A building protected by makeshift or scavenged shielding (-1)

## Section 6: Mutation

## Section 7: Compendium

### Talents

### Gear

## Section 8: GM'S Tools

## Section 9: Lore

### The Calamity

### The Glow